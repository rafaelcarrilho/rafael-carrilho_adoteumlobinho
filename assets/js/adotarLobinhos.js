const $ = document
const url = "https://treiamento-lobinhos-api.herokuapp.com/wolves"


fetch(url).then(resp => resp.json())
.then(resp => {
    let lastLobo = resp.length - 1
    let image = resp[lastLobo].image
    let name = resp[lastLobo].name
    let filho = $.createElement("div")
    let pai = $.querySelector("#lobinhoAdd")
    filho.innerHTML=
    "<div class=cardLoboID>" +
        "<div class=molde>"+   
            "<div class=ret><img src=" + image + ">" + "</div>" +
        "</div>" 
    pai.appendChild(filho)
})