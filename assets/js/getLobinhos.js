const url = "https://treiamento-lobinhos-api.herokuapp.com/wolves"
const $ = document
let pai = $.querySelector("#listaLobinhos")

let lob = 0
fetch(url).then(resp => resp.json()).then(resp => {
    for (let element = resp.length-4; element < resp.length; element++){
        let filho = $.createElement("li")
        let nome = resp[element].name
        let descricao = resp[element].description
        let idade = resp[element].age
        let img = resp[element].image
        let id = resp[element].id
        
        if (lob % 2 === 0){
            filho.innerHTML =
            "<div id=maeExemplos class=container>" +
                "<div id=exemplos>" +
                "<div class=cardExemplo>" +
                    "<div class=molde><div class=ret><img src=" + img + ">" + "</div></div>" +    
                    "<div class=informacoes>" + 
                        "<h5> Lobo:"  + nome + "</h5>" + 
                        "<h6> Age:" + idade + "</h6>" +
                        "<p> Descrição:" + descricao + "</p>" + 
                    "</div>"+
                    "<a href=pagAdocao.html id=butAdotarLobo name=" + id + "><button class=buttonLobo>Adotar</button></a>"+    
            "</div></div></div>"
        }else{
            filho.innerHTML =
            "<div id=maeExemplos class=container>" +
                "<div id=exemplos>" +
                "<div class=cardExemplo2>" +
                    "<div class=molde><div class=ret><img src=" + img + ">" + "</div></div>" +    
                    "<div class=informacoes2>" + 
                        "<h5> Lobo:"  + nome + "</h5>" + 
                        "<h6> Age:" + idade + "</h6>" +
                        "<p> Descrição:" + descricao + "</p>" + 
                    "</div>"+
                    "<a href=pagAdocao.html name=" + id +"><button class=buttonLobo>Adotar</button></a>"+    
            "</div></div></div>"
            }
          
        lob++
        pai.appendChild(filho)
    }    
})