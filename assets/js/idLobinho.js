
const url = "https://treiamento-lobinhos-api.herokuapp.com/wolves"
const $ = document
const pai = $.querySelector("#sec_7")


fetch(url).then(resp => resp.json())
.then(resp => {
    let filho = $.createElement("div")
    let lastLobo = resp.length - 1
    let image = resp[lastLobo].image
    let name = resp[lastLobo].name
    let description = resp[lastLobo].description
    let age = resp[lastLobo].age
    
    filho.innerHTML =
    "<div id=maeLobo class=container>" +
    
    "<p id=nomeLoboID> Lobo:"  + name + "</p>" +

    "<div class=cardLoboID>" +
        "<div class=molde>"+   
            "<div class=ret><img src=" + image + ">" + "</div>" +
        "</div>" +    
        "<div class=informacoesN>" +  
            
            "<p>" + description + "</p>" + 
        "</div>"+
            
    "</div>"+ 
    "</div>"
    
    let remBut = $.querySelector("#butApagar")
    remBut.value = resp[lastLobo].id
    let adotarBut = $.querySelector("#butAdotar")
    adotarBut.value = resp[lastLobo].id
    pai.appendChild(filho)
})


