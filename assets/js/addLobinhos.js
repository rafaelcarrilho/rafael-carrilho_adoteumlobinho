const url = "https://treiamento-lobinhos-api.herokuapp.com/wolves"
const $ = document

const myHeaders = {
    "content-Type": "application/JSON"
}

let butAdd = $.querySelector("#formSubmit")
butAdd.addEventListener("click", e => {
    e.preventDefault()
    let name = $.querySelector("#formNome").value
    let age = $.querySelector("#formAge").value
    let image = $.querySelector("#formImg").value
    let description = $.querySelector("#formDescricao").value

    let post = {
        "name": name,
        "age": age,
        "image": image,
        "description": description
    }
    let fetchConfig = {
        method: "POST",
        headers: myHeaders,
        body: JSON.stringify(post)
    }
    fetch(url, fetchConfig).then(console.log).catch(() => alert("Err"))




})